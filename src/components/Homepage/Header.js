import '../../style/Homepage.css'
import {Link} from "react-router-dom"
import {Grid, TextField} from "@material-ui/core"

function Header() {
    return (
        <Grid container id="header-container">
            <Grid item md={3} sm={false} xs={false}></Grid>
            <Grid style={{display: "flex"}} item md={6} sm={12} xs={12}>
                <div id="header-logo">
                    <Link to="/" id="logo"></Link>
                </div>
                <TextField variant="outlined" size="small" placeholder="Nhập tên phim cần tìm"/>
            </Grid>
            <Grid item md={3} sm={false} xs={false}></Grid>
        </Grid>
    );
}

export default Header;