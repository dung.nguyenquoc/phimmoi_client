import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import HomePage from "./views/Homepage/Homepage"

function Routes() {
    return (
        <BrowserRouter>
            <Route exact path="/" component={HomePage} />
        </BrowserRouter>
    );
}

export default Routes;